# I2B Trinity Architecture NodeJs Guide() {
****

*Esta guía trata sobre dar un buen enfoque general al buen uso de la arquitectura Trinity*

> ** Nota **: esta guía asume un conocimiento básico del nodo js también asume que está instalando [Node JS] (https://nodejs.org/es/)

# Requisitos

Para ejecutar el entorno, debe tener al menos la siguiente stack:
- Docker

# Clonar Projecto
git clone : 

# Estructura de archivos

```
> i2btemplate-nodejs-ta
   > src     
    > api   
    > api-client       
    > app
    > auxiliar
    > auxiliar-detail             
    > domain      
    > domain-detail
  |-app.js         
  - .dockerignore
  - .env   
  - Dockerfile    
  - package.json  
  - README.md
```

# Configuración Inicial

## Variables de entorno

Cree un archivo con variable de entorno para configurar los servicios.

```
cp .env
```
```
~/path/i2btemplate-nodejs-ta $ cp .env
```
### Ejecutando el siguiente comando. Esto creará imágenes y volúmenes de la ventana acoplable que se ejecutarán en los contenedores.

### Si desea construir sus imágenes antes de comenzar sus contenedores.
```
$ docker-compose up --build
```
## docker-compose
```
$ docker-compose up
```

## Run a command in a running container
docker exec -it server bash

