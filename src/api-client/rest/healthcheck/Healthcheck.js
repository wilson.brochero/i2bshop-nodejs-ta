'use strict';

const express = require('express');
const router = express.Router();

router.use('/healthcheck', require('../../../app/healthcheck/HealthCheckService'));

module.exports = router;
