'use strict';

const express = require('express');
const jwt = require('jsonwebtoken');

const router = express.Router();

const { authentication } = require('../../../api/login/LoginService');
const { ExceptionUserNotFound, ExceptionUserFormatData, ExceptionUserPassword } = require('../../../app/exceptions/LoginExceptions');

router.post('/authentication', async (request, response) => {

    try {
        const {
            email,
            password
        } = request.body;

        const loginData = { email, password };

        const userData = await authentication(loginData);

        const token = jwt.sign({
            user: userData
        }, process.env.SEED, { expiresIn: process.env.EXPIRES_TOKEN });

        response.status(200).json({ userData, token });

    }
    catch (exception) {
        if (exception instanceof ExceptionUserNotFound)
            response.status(400).json({ msg: exception.mensaje });
        if (exception instanceof ExceptionUserFormatData)
            response.status(400).json({ msg: exception.mensaje });
        if (exception instanceof ExceptionUserPassword)
            response.status(401).json({ msg: exception.mensaje });
        else
            response.status(500).json({ msg: exception });
    }

});

module.exports = router;