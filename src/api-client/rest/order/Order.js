"use strict";

const express = require("express");

const { createOrder } = require("../../../api/order/CreateOrderService");
const { validationToken } = require("../../../app/security/Authentication");
const { ExceptionUserNotFound } = require('../../../app/exceptions/UserExceptions');
const { ExceptionProductNotFound, ExceptionValidateStock } = require('../../../app/exceptions/ProductExceptions');

const router = express.Router();

router.post('/createOrder', validationToken, async (request, response) => {

    try {

        const {
            email,
            sku,
            quantity
        } = request.body;

        const orderData = { email, sku, quantity };

        const createOrderResponse = await createOrder(orderData);

        response.status(200).json({ createOrderResponse });

    }
    catch (exception) {
        if (exception instanceof ExceptionUserNotFound)
            response.status(400).json({ msg: exception.mensaje });
        if (exception instanceof ExceptionProductNotFound)
            response.status(400).json({ msg: exception.mensaje });
        if (exception instanceof ExceptionValidateStock)
            response.status(400).json({ msg: exception.mensaje });
        else
            response.status(500).json({ msg: exception });
    }

});

module.exports = router;
