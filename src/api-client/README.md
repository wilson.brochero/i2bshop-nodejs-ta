# Descripción

- Se debe crear un paquete para cada protocolo que desee exponer. 
- Routes.js en un archivo que sirve para *enrutar* los arhivos js dentro de *rest*, *soap*.
- Dentro de cada paquete por protocolo, se creará un archivo que tendrá la el endpoit de las acciones del api, *Ejemplo* (rest/Login), (rest/User)

# Estructura de Arhivos

```
> i2btemplate-nodejs-ta
   > src
        > api-client
            > rest
                |- Login.js
            > soap
                |- Login.js
            |- Routes.js

```

