'use strict';

const express = require('express');
const router = express.Router();

const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
router.use(bodyParser.json());

router.use(require('./rest/login/Login'));
router.use(require('./rest/order/Order'));

//Healthcheck
router.use(require('./rest/Healthcheck/Healthcheck'));

module.exports = router;
