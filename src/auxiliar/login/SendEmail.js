'use strict';

const emailService = require('../../auxiliar-details/email/SendEmailImpl');

let sendMail = async (loginData) => {
    try {
        return await emailService.sendMail(loginData);
    }
    catch (exception) {
        throw exception;
    }
}

module.exports = {
    sendMail
}
