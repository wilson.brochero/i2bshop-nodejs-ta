'use strict';

const loginRepository = require('../../auxiliar-details/login/LoginRepositoryImpl');

let authentication = async (loginData) => {
    try {
        return await loginRepository.authentication(loginData);
    }
    catch (exception) {        
        throw exception;
    }
}

module.exports = {
    authentication
}
