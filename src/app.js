'use strict';

const express = require('express');
const mongoose = require('mongoose');
const chalk = require('chalk');
const apiRouter = require('./api-client/Routes');

const { appConfig } = require('./app/config/Environment');

require('dotenv').config();

const app = express();

mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE_CONECCTION, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
})
  .then(() => {
    console.log(
      chalk.black.bgYellow.bold(
        ' La conexión a la base de datos se ha realizado correctamente '
      )
    );

    app.listen(process.env.PORT, () => {
      console.log(
        chalk.white.bgRed.bold(' servidor corriendo en ') +
        chalk.white.bgRed.underline.bold(
          `${process.env.HOST_NAME}:${process.env.PORT} `
        )
      );
    });
  })
  .catch((err) => console.log(chalk.red.underline(err)));

app.use('/api', apiRouter);
