'use strict';

const loginRepository = require('../../api-details/login/LoginServiceImpl');

const authentication = async (loginData) => {
    try {
        return await loginRepository.authentication(loginData);
    }
    catch (exception) {        
      throw exception;
    }
}

module.exports = {
    authentication
}
