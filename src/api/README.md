# Descripción 

- Este paquete contiene la capacidad de acceder a la lógica empresarial.
- Se debe crear un subpaquete para cada acceso al paquete *dominio* o al paquete *auxiliar*.
- Los archivos creados dentro de /api deben llevar la terminación *Controller*
- Acá se harán las validaciones de datos
- La carpeta *healthcheck* tiene un trato especial, debe ir en todos los proyectos

# Ejemplo Técnico

```
    let authentication = async (request, response) => {
        try {

            let {
                email,
                password
            } = request.body;

            const loginData = { email, password };

            let userData = await loginRepository.authentication(loginData);

            if (!userData) {
                response.status(401).json({ msg: 'El usuario no existe.' });
            }

            if (!bcrypt.compareSync(password, userData.password)) {
                response.status(401).json({ msg: 'Contraseña incorrecta' });
            }

            let token = jwt.sign({
                user: userData
            }, process.env.SEED, { expiresIn: process.env.EXPIRES_TOKEN });

            response.status(200).json({ userData, token });

        }
        catch (error) {
            response.status(500).json();
        }
    }
```