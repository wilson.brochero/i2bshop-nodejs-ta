'use strict';

const orderRepository = require('../../api-details/order/CreateOrderServiceImpl');

const createOrder = async (orderData) => {
    try {
        return await orderRepository.createOrder(orderData);
    }
    catch (exception) {
        throw exception;
    }
}
module.exports = {
    createOrder
}
