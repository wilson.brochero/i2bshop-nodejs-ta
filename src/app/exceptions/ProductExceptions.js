'use strict';

function ExceptionProductNotFound(mensaje) {
    this.mensaje = mensaje;
    this.nombre = "ExceptionProductNotFound";
}

function ExceptionValidateStock(mensaje) {
    this.mensaje = mensaje;
    this.nombre = "ExceptionValidateStock";
}

module.exports = {
    ExceptionProductNotFound,
    ExceptionValidateStock
}