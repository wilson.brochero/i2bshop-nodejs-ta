'use strict';

function ExceptionUserNotFound(mensaje) {
    this.mensaje = mensaje;
    this.nombre = "ExceptionUsuario";
}

function ExceptionUserFormatData(mensaje) {
    this.mensaje = mensaje;
    this.nombre = "ExceptionUserFormatData";
}

function ExceptionUserPassword(mensaje) {
    this.mensaje = mensaje;
    this.nombre = "ExceptionUserPassword";
}

 module.exports = {
    ExceptionUserNotFound,
    ExceptionUserFormatData,
    ExceptionUserPassword
}
