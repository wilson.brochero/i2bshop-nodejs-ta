'use strict';

function ExceptionUserNotFound(mensaje) {
    this.mensaje = mensaje;
    this.nombre = "ExceptionUserNotFound";
}

 module.exports = {
    ExceptionUserNotFound
}
