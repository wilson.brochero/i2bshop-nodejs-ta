'use strict';

let create = {
    name: 'required',
    email: 'required|email',
    age: 'min:18'
};

let update = {
    name: 'required',
    email: 'required|email',
    age: 'min:18'
};

let get = {
    name: 'required',
    email: 'required|email',
    age: 'min:18'
};


module.exports = { create, update, get };