'use strict';

let rules = {
    password: 'required',
    email: 'required|email'
};

let messages = {
    required: 'Campo :attribute requerido.',
    email: 'Formato de :attribute incorrecto.'
};

module.exports = {
    rules,
    messages,
};
