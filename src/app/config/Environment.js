const config = {
    appConfig:{
        PORT: process.env.PORT,
        HOST_NAME: process.env.HOST_NAME,
        URL_CONECCTION : process.env.DATABASE_CONECCTION
    }
}

module.exports  = config;
