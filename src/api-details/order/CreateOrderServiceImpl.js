'use strict';

const userRepository = require('../../domain/user/UserRepository');
const productRepository = require('../../domain/product/ProductRepository');
const orderRepository = require('../../domain/order/OrderRepository');
const { response } = require('express');

const createOrder = async (orderData) => {
    try {

        const userData = await userRepository.getUserInformation(orderData.email); // 

        const productData = await productRepository.getProductInformation(orderData.sku);

        await productRepository.validateStock(orderData.sku, orderData.quantity);

        const completeOrderData = {
            email: userData.email,
            address: userData.address,
            sku: orderData.sku,
            quantity: orderData.quantity,
            totalAmount: orderData.quantity * productData.price
        };

        return await orderRepository.createOrder(completeOrderData);

    }
    catch (exception) {
        throw exception;
    }
}

module.exports = {
    createOrder
}
