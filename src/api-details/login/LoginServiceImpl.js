'use strict';

const loginRepository = require('../../auxiliar/login/LoginRepository');

const authentication = async (loginData) => {
    try {
        return await loginRepository.authentication(loginData);
    }
    catch (exception) {
        throw exception;
    }
}

module.exports = {
    authentication
}
