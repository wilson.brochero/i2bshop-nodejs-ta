# Descripción 

*El paquete domain es la abstracción de los metodos del paquete domain-detail.*

- Dentro de *domain* se deberan agregar subpaquetes y js relacionados con el negocio, **Ejemplo:** *domain/user* , *domain/plan*. 
- Los nombres de los js dentro de domain debel llevar la terminación *Repository* , **Ejemplo :** *UserReporsitory*.
