'use strict';

const productRepository = require('../../domain-details/order/OrderRepositoryImpl');

const createOrder = async (orderData) => {
  try {
    return await productRepository.createOrder(orderData);
  } catch (exception) {
    throw exception;
  }
};

module.exports = {
  createOrder
};
