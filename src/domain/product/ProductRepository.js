'use strict';

const productRepository = require('../../domain-details/product/ProductRepositoryImpl');


let getProductInformation = async (sku) => {
  try {
    return await productRepository.getProductInformation(sku);
  } catch (exception) {
    throw exception;
  }
};

let validateStock = async (sku, quantity) => {
  try {
    return await productRepository.validateStock(sku, quantity);
  } catch (exception) {
    throw exception;
  }
};

module.exports = {
  getProductInformation,
  validateStock
};
