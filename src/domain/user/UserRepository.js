'use strict';

const userRepository = require('../../domain-details/user/UserRepositoryImpl');


const getUserInformation = async (email) => {
  try {
    return await userRepository.getUserInformation(email);
  } catch (exception) {
    throw exception;
  }
};

module.exports = {
  getUserInformation,
};
