'use strict';

const ordesModel = require('./Order');
const { ExceptionProductNotFound } = require('../../app/exceptions/OrderExceptions');


const createOrder = async (orderData) => {
  try {

    let newOrder = new ordesModel(orderData);

    return await newOrder.save();

  } catch (exception) {
    console.log(exception);
    throw exception;
  }
};

module.exports = {
  createOrder
};
