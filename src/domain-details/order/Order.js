'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ordesSchema = new Schema({
  email: {
    type: String,
    require: [true, 'This field is required'],
  },
  address: {
    type: String,
    require: [true, 'This field is required'],
  },
  sku: {
    type: String,
    require: [true, 'This field is required'],
  },
  totalAmount: {
    type: Number,
    required: [true, 'This field is required'],
  },
});

module.exports = mongoose.model('ordes', ordesSchema);
