'use strict';

const userModel = require('./User');
const { ExceptionUserNotFound } = require('../../app/exceptions/UserExceptions');


const getUserInformation = async (email) => {
  try {

    let userData = await userModel.findOne({ email: email });

    if (!userData)
      throw new ExceptionUserNotFound('El usuario no existe.');

    return userData;

  } catch (exception) {
    throw exception;
  }
};

module.exports = {
  getUserInformation,
};
