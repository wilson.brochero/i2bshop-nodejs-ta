'use strict';

const productsModel = require('./Product');
const { ExceptionProductNotFound, ExceptionValidateStock } = require('../../app/exceptions/ProductExceptions');

const getProductInformation = async (sku) => {
  try {

    let productData = await productsModel.findOne({ sku: sku });

    if (!productData)
      throw new ExceptionProductNotFound('El sku no existe.');

    return productData;

  } catch (exception) {
    throw exception;
  }
};

const validateStock = async (sku, quantity) => {
  try {

    let productData = await productsModel.findOne({ sku: sku });

    if (!productData)
      throw new ExceptionProductNotFound('El sku no existe.');

    if (quantity > productData.quantity)
      throw new ExceptionValidateStock('No hay articulos suficientes.');

    return productData;

  } catch (exception) {
    throw exception;
  }
};

module.exports = {
  getProductInformation,
  validateStock
};
