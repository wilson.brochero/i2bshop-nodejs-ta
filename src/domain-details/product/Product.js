'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productsSchema = new Schema({
  name: {
    type: String,
    require: [true, 'This field is required'],
  },
  price: {
    type: String,
    require: [true, 'This field is required'],
  },
  quantity: {
    type: String,
    required: [true, 'This field is required'],
  },
  sku: {
    type: String,
    require: [true, 'This field is required'],
  },
});


module.exports = mongoose.model('products', productsSchema);
