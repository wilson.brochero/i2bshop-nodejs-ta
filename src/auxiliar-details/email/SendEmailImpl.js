var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'wilsonbrochero@gmail.com',
        pass: 'xxxxxxx'
    }
});

var mailOptions = {
    from: 'wilsonbrochero@gmail.com',
    to: 'wilsonbrochero@gmail.com',
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
};

const sendMail = async (loginData) => {
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    })
}

module.exports = {
    sendMail,
};
