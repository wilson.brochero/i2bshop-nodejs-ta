'use strict';
const bcrypt = require('bcrypt');
const Validator = require('validatorjs');

const userdb = require('../../domain-details/user/User');
const { ExceptionUserNotFound, ExceptionUserFormatData, ExceptionUserPassword } = require('../../app/exceptions/LoginExceptions');
const { rules, messages } = require('../../app/rules/LoginRules');

const authentication = async (loginData) => {
  try {

    let validation = new Validator(loginData, rules, messages);

    if (validation.fails()) {
      throw new ExceptionUserFormatData(validation.errors.all());
    }

    let userData = await userdb.findOne({
      email: loginData.email,
    });

    if (!userData) {
      throw new ExceptionUserNotFound('El usuario no existe.');
    }

    if (!bcrypt.compareSync(loginData.password, userData.password)) {
      throw new ExceptionUserPassword('Contraseña incorrecta.');
    }

    return userData;

  } catch (exception) {
    throw exception;
  }
};

module.exports = {
  authentication,
};
