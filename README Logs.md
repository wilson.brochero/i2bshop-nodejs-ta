# I2B Logging Bets Practices
****

*Esta guía trata sobre buenas practicas para el tratamiento de logs en la aplicaciones back-end*

# Tipos de registro

*Los tipos de registro hacen referencia al grupo información que se desea consignar en los archivos log ya sea durante el flujo normal de cada proceso desde el inicio hasta el final y los posibles errores que ocurrente durante estos, tenemos los siguientes tipos:*

* info
* error
* warn

*Cuando utilizar cada uno*

## INFO
*En los info y en ningun otro se deberan imprimir _CONTRASEÑAS, TOKENS_ o cualquier otro dato que permita acceso a la apliaciones o acceso a base de datos, ect.*


## ERROR 
*Los error deberan ser utilizados dentro de los espacios de captura de excepciones o errores según el lenguaje utilizado*

### Ejemplo:
```
try {
    throw new Error('Ocurrió un error intentado XXXXX XXXXX !');
  } catch (error) {
    console.error('Mensaje de error : ', error);    
  }
```
*Los mensaje de error deberán ir acompañado con el objeto que encapsula la causa del error para tener mas detalles de echo*

### Ejemplo:
```
  console.error('Mensaje de error : ', error);
```


## WARN 


